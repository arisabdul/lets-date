<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="antialiased bg-fixed bg-cover h-screen" style="background-image: url({{ asset('assets/banner.jpeg') }})">
    @include('livewire.layout.navigation')

    <center class="w-3/4 mx-auto h-full flex flex-col justify-center items-center">
        <h1 class="font-bold text-white">Swipe Right
            <sup>
                <span class="text-xl p-2 px-3 border-4 rounded-full">R</span>
            </sup>
        </h1>
        <a href="{{ route('register') }}" class="btn btn-gradient mt-2 text-2xl">
            Create Account
        </a>
    </center>

    @livewireScripts
</body>

</html>
